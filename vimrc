filetype off
call pathogen#runtime_append_all_bundles()
filetype plugin indent on

function! s:AsciidocSettings()
    set spell
endfunction

set nocompatible

set modelines=0

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2

set relativenumber
au InsertEnter * :set nu
au InsertLeave * :set rnu
au FocusLost * :set nu
au FocusGained * :set rnu

set undofile
let mapleader = ","
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
nnoremap <leader><space> :noh<cr>
nnoremap <tab> %
vnoremap <tab> %
set textwidth=79
set formatoptions=qrn1
set colorcolumn=85

inoremap jj <ESC>

nmap <C-t> :TlistToggle<CR>

nmap ,f :FufFileWithCurrentBufferDir<CR>
nmap ,b :FufBuffer<CR>
nmap ,t :FufTag<CR>
nmap ,tf :FufTaggedFile<CR>

nmap qa :qa!<CR>

colorscheme desert

set tags=./tags;
let g:easytags_dynamic_files = 2

let tlist_asciidoc_settings = 'asciidoc;t:tag,l:title'

autocmd FileType asciidoc call s:AsciidocSettings()
autocmd FileType html,htmldjango,jinjahtml,eruby,mako let b:closetag_html_style=1
autocmd FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako source ~/.vim/bundle/closetag/plugin/closetag.vim
